EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Mechanical:MountingHole H1
U 1 1 5C094873
P 1600 1200
F 0 "H1" H 1700 1246 50  0000 L CNN
F 1 "MountingHole" H 1700 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1600 1200 50  0001 C CNN
F 3 "~" H 1600 1200 50  0001 C CNN
	1    1600 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5C094901
P 1750 1200
F 0 "H2" H 1850 1246 50  0000 L CNN
F 1 "MountingHole" H 1850 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1750 1200 50  0001 C CNN
F 3 "~" H 1750 1200 50  0001 C CNN
	1    1750 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5C094923
P 1900 1200
F 0 "H3" H 2000 1246 50  0000 L CNN
F 1 "MountingHole" H 2000 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1900 1200 50  0001 C CNN
F 3 "~" H 1900 1200 50  0001 C CNN
	1    1900 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5C09493B
P 2050 1200
F 0 "H4" H 2150 1246 50  0000 L CNN
F 1 "MountingHole" H 2150 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2050 1200 50  0001 C CNN
F 3 "~" H 2050 1200 50  0001 C CNN
	1    2050 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H5
U 1 1 5C094969
P 2200 1200
F 0 "H5" H 2300 1246 50  0000 L CNN
F 1 "MountingHole" H 2300 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2200 1200 50  0001 C CNN
F 3 "~" H 2200 1200 50  0001 C CNN
	1    2200 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H6
U 1 1 5C09498D
P 2350 1200
F 0 "H6" H 2450 1246 50  0000 L CNN
F 1 "MountingHole" H 2450 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 2350 1200 50  0001 C CNN
F 3 "~" H 2350 1200 50  0001 C CNN
	1    2350 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H7
U 1 1 5C094A0D
P 1300 1200
F 0 "H7" H 1400 1246 50  0000 L CNN
F 1 "MountingHole" H 1400 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1300 1200 50  0001 C CNN
F 3 "~" H 1300 1200 50  0001 C CNN
	1    1300 1200
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H8
U 1 1 5C094A14
P 1450 1200
F 0 "H8" H 1550 1246 50  0000 L CNN
F 1 "MountingHole" H 1550 1155 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 1450 1200 50  0001 C CNN
F 3 "~" H 1450 1200 50  0001 C CNN
	1    1450 1200
	1    0    0    -1  
$EndComp
$EndSCHEMATC
